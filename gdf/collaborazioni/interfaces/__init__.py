# -*- coding: utf-8 -*-
from zope.interface import Interface


class IConsulenzeTabularView(Interface):
    '''
    Marker interface for consulenze_tabular_view and similar
    '''
