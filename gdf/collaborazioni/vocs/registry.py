# -*- coding: utf-8 -*-
from gdf.collaborazioni.registry.panel import ICollaborazioniSettings
from plone.registry.interfaces import IRegistry
from zope.component import getUtility


def get_collaborazioni_settings():
    '''
    Get the RecordsProxy from the Registry
    '''
    registry = getUtility(IRegistry)
    return registry.forInterface(ICollaborazioniSettings)


def get_from_collaborazioni_registry(records_key, fallback=None):
    '''
    Get the given records_key from the collaborazioni RecordsProxy

    :param records_key: The key of the Records in the RecordsProxy
    '''
    records_proxy = get_collaborazioni_settings()
    return getattr(records_proxy, records_key, fallback)


def sort_by_title(objs):
    '''
    Sort objects by title

    :param objs: objects that have a title attribute!
    '''
    return sorted(objs, key=lambda x: x.title)
