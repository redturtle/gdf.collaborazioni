# -*- coding: utf-8 -*-
from five import grok
from gdf.collaborazioni.vocs.base import SimpleSafeVocabulary
from gdf.collaborazioni.vocs.registry import get_from_collaborazioni_registry
from zope.schema.interfaces import IContextSourceBinder
from zope.schema.vocabulary import SimpleTerm


@grok.provider(IContextSourceBinder)
def Vocabulary(context):
    '''
    Get from the registry the value for modalita_individuazione_beneficiario
    :param context: a Plone object
    '''
    key = 'modalita_individuazione_beneficiario'
    fallback = ()
    objs = get_from_collaborazioni_registry(key, fallback)
    terms = [SimpleTerm(obj.key, obj.key, obj.value)
             for obj in objs]
    terms.sort(key=lambda x: x.title)
    return SimpleSafeVocabulary(terms)
