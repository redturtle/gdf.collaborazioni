# -*- coding: utf-8 -*-
from datetime import date
from plone.app.z3cform import widget
import z3c.form
import zope


class IDateField(widget.IDateField):
    '''
    Marker interface to override registration
    '''


class DateWidget(widget.DateWidget):
    '''
    Fixes the formatted value
    '''
    format = 'long'

    @property
    def formatter(self):
        '''
        The original formatter is hardcoded and uses short :(
        '''
        return self.request.locale.dates.getFormatter("date",
                                                      self.format)

    @property
    def formatted_value(self):
        '''
        Make a little bit easier to format the value of the date
        '''
        if  not self.mode == 'display':
            return super(DateWidget, self).formatted_value
        if self.value == ('', '', ''):
            return ''
        date_value = date(*map(int, self.value))
        if date_value.year > 1900:
            return self.formatter.format(date_value)
        # due to fantastic datetime.strftime we need this hack
        # for now ctime is default
        return date_value.ctime()


@zope.component.adapter(zope.schema.interfaces.IField, z3c.form.interfaces.IFormLayer)
@zope.interface.implementer(z3c.form.interfaces.IFieldWidget)
def DateFieldWidget(field, request):
    return z3c.form.widget.FieldWidget(field, DateWidget(request))
