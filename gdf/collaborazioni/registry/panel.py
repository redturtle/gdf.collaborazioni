# -*- coding: utf-8 -*-
from gdf.collaborazioni import collaborazioniMessageFactory as _
from plone.app.registry.browser import controlpanel
from z3c.form import interfaces
from zope import schema
from zope.interface import Interface
from gdf.collaborazioni.registry.fields import PersistentObject, IKeyValuePair


class ICollaborazioniSettings(Interface):
    """Global collaborazioni settings. This describes records stored in the
    configuration registry and obtainable via plone.registry.
    """

    uffici_responsabili_procedimento = schema.Tuple(
            title=_(u"uffici_responsabili_procedimento_title",
                    "Uffici responsabili del procedimento"),
            value_type=PersistentObject(IKeyValuePair,
                                        title=_(u"key_value_pair_title",
                                                "Coppie chiave/valore")),
            required=False,
            default=(),
            missing_value=(),
    )

    modalita_individuazione_beneficiario = schema.Tuple(
            title=_(u"modalita_individuazione_beneficiario_title",
                    u"Modalità di individuazione del beneficiario"),
            value_type=PersistentObject(IKeyValuePair,
                                        title=_(u"key_value_pair_title",
                                                "Coppie chiave/valore")),
            required=False,
            default=(),
            missing_value=(),
    )


class CollaborazioniSettingsEditForm(controlpanel.RegistryEditForm):

    schema = ICollaborazioniSettings
    label = _(u"Collaborazioni settings")
    description = _(u"""""")

    def updateFields(self):
        super(CollaborazioniSettingsEditForm, self).updateFields()

    def updateWidgets(self):
        super(CollaborazioniSettingsEditForm, self).updateWidgets()


class CollaborazioniSettingsControlPanel(controlpanel.ControlPanelFormWrapper):
    form = CollaborazioniSettingsEditForm
