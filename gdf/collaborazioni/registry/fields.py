# -*- coding: utf-8 -*-
from gdf.collaborazioni import collaborazioniMessageFactory as _
from plone.registry.field import PersistentField
from z3c.form.object import registerFactoryAdapter
from zope import schema
from zope.interface import Interface, implements


class IKeyValuePair(Interface):
    '''
    A simple Key Value object
    '''
    key = schema.ASCIILine(title=_(u"key_title", u"Chiave"),)
    value = schema.TextLine(title=_(u"value_title", "Valore"),)


class KeyValuePair(object):
    '''
    A simple Key Value object
    '''
    implements(IKeyValuePair)


class PersistentObject(PersistentField, schema.Object):
    pass

registerFactoryAdapter(IKeyValuePair, KeyValuePair)
