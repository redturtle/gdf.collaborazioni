# -*- coding: utf-8 -*-
from plone.app.layout.viewlets import ViewletBase


class Viewlet(ViewletBase):
    '''
    Class for the consulenze search viewlet
    '''
