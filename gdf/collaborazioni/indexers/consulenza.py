# -*- coding: utf-8 -*-
from gdf.collaborazioni.models.consulenza import IConsulenza
from plone.indexer.decorator import indexer


@indexer(IConsulenza)
def searchable_text(context, **kw):
    '''
    Main indexer for consulenza searchable text
    '''
    consulenza_view = context.restrictedTraverse('@@consulenza_view')
    intestatario = context.intestatario.encode('utf8')
    ufficio = consulenza_view.ufficio_responsabile_procedimento_hr
    ufficio = ufficio.encode('utf8')
    ragione = context.oggetto_rapporto.encode('utf8')
    searchable_text = [context.SearchableText(), ufficio, intestatario,
                       ragione]
    return " ".join(searchable_text)
