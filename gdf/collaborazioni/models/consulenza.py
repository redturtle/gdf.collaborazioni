# -*- coding: utf-8 -*-
from datetime import date
from plone.directives import form
from plone.dexterity.content import Container
from zope.interface import implements


def get_date_for_display(value, fallback=u'\u2014'):
    '''
    Try to return a date in the format %d/%m/%Y, else return the fallback value

    :param value: hopefully a datetime.date object
    :param fallback: the value returned if formatting fails
    '''
    if isinstance(value, date):
        return value.strftime('%d/%m/%Y')
    else:
        return fallback


class IConsulenza(form.Schema):
    '''
    The interface binding the supermodel for consulenza
    '''
    form.model('consulenza.xml')


class Consulenza(Container):
    '''
    Consulenza class
    '''
    implements(IConsulenza)

    @property
    def durata_incarico(self):
        '''
        Overrides durata incarico
        '''
        dal = get_date_for_display(self.data_inizio_incarico)
        al = get_date_for_display(self.data_fine_incarico)
        return u"Dal %s al %s" % (dal, al)

    @property
    def durata_incarico_from(self):
        '''
        Overrides durata incarico from
        '''
        dal = get_date_for_display(self.data_inizio_incarico)
        return u"Dal %s" % dal

    @property
    def durata_incarico_to(self):
        '''
        Overrides durata incarico to
        '''
        al = get_date_for_display(self.data_fine_incarico)
        return u"al %s" % al
