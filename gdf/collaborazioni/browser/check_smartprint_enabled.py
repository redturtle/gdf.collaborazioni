# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView


class View(BrowserView):
    """
    Check if the current view is SmartPrintNG enabled
    """
    allowed_templates = ('consulenze_tabular_view',
                         'consulenze_compact_tabular_view',
                         'consulenze_2015_tabular_view')

    def check_allowed_views(self, template_id):
        """
        check if the given template_id is in allowed_templates
        """
        return template_id in self.allowed_templates

    def __call__(self):
        """
        """
        steps = getattr(self.request, 'steps', [])
        if steps:
            return self.check_allowed_views(steps[-1])
