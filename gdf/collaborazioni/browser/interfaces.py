# -*- coding: utf-8 -*-
from gdf.theme.browser.interfaces import IThemeSpecific as IGDFThemeSpecific


class IThemeSpecific(IGDFThemeSpecific):
    """Marker interface that defines a Zope 3 browser layer.
    """
