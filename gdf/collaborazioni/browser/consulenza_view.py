# -*- coding: utf-8 -*-
from plone.dexterity.browser.view import DefaultView
from plone.memoize.view import memoize
from gdf.collaborazioni.vocs import (modalita_individuazione_beneficiario,
                                     uffici_responsabili_procedimento)


class View(DefaultView):
    '''
    Customized view for applicazione dexterity type
    '''

    def update(self):
        '''
        Updates this view
        '''
        super(View, self).update()

    @memoize
    def get_uffici_responsabili_procedimento_voc(self):
        '''
        Returns the vocabulary uffici_responsabili_procedimento
        '''
        return uffici_responsabili_procedimento.Vocabulary(self.context)

    @property
    def ufficio_responsabile_procedimento_hr(self):
        '''
        Return the ufficio responsabile del procedimento in a human readable
        form
        '''
        voc = self.get_uffici_responsabili_procedimento_voc()
        value = self.context.ufficio_responsabile_procedimento
        return voc.getSafeTermTitle(value)

    @memoize
    def get_modalita_individuazione_beneficiario_voc(self):
        '''
        Returns the vocabulary modalita_individuazione_beneficiario
        '''
        return modalita_individuazione_beneficiario.Vocabulary(self.context)

    @property
    def modalita_individuazione_beneficiario_hr(self):
        '''
        Return the ufficio responsabile del procedimento in a human readable
        form
        '''
        voc = self.get_modalita_individuazione_beneficiario_voc()
        value = self.context.modalita_individuazione_beneficiario
        return voc.getSafeTermTitle(value)
