# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from gdf.collaborazioni.interfaces import IConsulenzeTabularView
from gdf.collaborazioni.vocs import (uffici_responsabili_procedimento,
    modalita_individuazione_beneficiario)
from plone.memoize.view import memoize
from zope.interface.declarations import implements

# quote chars is copied from plone.app.search, available since Plone 4.2
# We should accept both a simple space, unicode u'\u0020 but also a
# multi-space, so called 'waji-kankaku', unicode u'\u3000'
MULTISPACE = u'\u3000'.encode('utf-8')


def quote_chars(s):
    # We need to quote parentheses when searching text indices
    if '(' in s:
        s = s.replace('(', '"("')
    if ')' in s:
        s = s.replace(')', '")"')
    if MULTISPACE in s:
        s = s.replace(MULTISPACE, ' ')
    return s


class View(BrowserView):
    '''
    The view for listing consulenza objects with less fields
    '''
    implements(IConsulenzeTabularView)

    compact = False
    allowed_types = ['consulenza']

    @property
    @memoize
    def content_filter(self):
        '''
        Check the content filter for this object
        '''
        content_filter = {'portal_type': 'consulenza'}
        searchable_text = self.request.get('SearchableText', '')
        if searchable_text:
            content_filter['SearchableText'] = quote_chars(searchable_text)
        return content_filter

    @memoize
    def get_modalita_vocabulary(self):
        '''
        Get the modalita vocabulary
        '''
        return modalita_individuazione_beneficiario.Vocabulary(self.context)

    @memoize
    def get_uffici_vocabulary(self):
        '''
        Get the uffici vocabulary
        '''
        return uffici_responsabili_procedimento.Vocabulary(self.context)

    def modalita_hr(self, obj):
        '''
        Return the modalita_individuazione_beneficiario in a human readable
        form

        :param obj: a consulenza object
        '''
        voc = self.get_modalita_vocabulary()
        value = obj.modalita_individuazione_beneficiario
        return voc.getSafeTermTitle(value)

    def ufficio_hr(self, obj):
        '''
        Return the ufficio_responsabile_procedimento in a human readable form

        :param obj: a consulenza object
        '''
        voc = self.get_uffici_vocabulary()
        value = obj.ufficio_responsabile_procedimento
        return voc.getSafeTermTitle(value)


class ViewCompact(View):
    '''
    The view for listing consulenza objects with less fields
    '''
    compact = True

class View2015(View):
    '''
    The view for listing consulenza 2015 objects
    '''

